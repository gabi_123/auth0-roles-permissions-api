<?php
// src/Main.php

namespace App;

use Auth0\SDK\Exception\InvalidTokenException;
use Auth0\SDK\Helpers\JWKFetcher;
use Auth0\SDK\Helpers\Tokens\AsymmetricVerifier;
use Auth0\SDK\Helpers\Tokens\TokenVerifier;
use Kodus\Cache\FileCache;

class Main {

    protected $issuer;
    protected $audience;
    protected $token;
    protected $tokenInfo;

    public function __construct( $issuer, $audience ) {
        $this->issuer = $issuer;
        $this->audience = $audience;
    }

    public function setCurrentToken($token) {
        $cacheHandler = new FileCache('./cache', 600);
        $jwksUri      = $this->issuer . '.well-known/jwks.json';

        $jwksFetcher   = new JWKFetcher($cacheHandler, [ 'base_uri' => $jwksUri ]);
        $sigVerifier   = new AsymmetricVerifier($jwksFetcher);
        $tokenVerifier = new TokenVerifier($this->issuer, $this->audience, $sigVerifier);

        try {
            $this->tokenInfo = $tokenVerifier->verify($token);
            $this->token = $token;
        }
        catch(InvalidTokenException $e) {
            // Handle invalid JWT exception ...
            echo 'InvalidTokenException: ' . $e;
        }
    }

    // This endpoint doesn't need authentication
    public function publicEndpoint() {
        return array(
            "status" => "ok",
            "message" => "Hello from a public endpoint! You don't need to be authenticated to see this."
        );
    }

    public function privateEndpoint() {
        return array(
            "status" => "ok",
            "message" => "Hello from a private endpoint! You need to be authenticated to see this."
        );
    }

    public function checkScope($scope){
        if ($this->tokenInfo && array_key_exists('scope',$this->tokenInfo)){
            $token_info = $this->tokenInfo;
            $scopes = explode(" ",$token_info['scope']);
            foreach ($scopes as $s){
                if ($s === $scope)
                    return true;
            }
        }

        return false;
    }

    public function privateScopedEndpoint() {
        return array(
            "status" => "ok",
            "message" => "Hello from a private endpoint! You need to be authenticated and have a scope of read:users to see this."
        );
    }
}