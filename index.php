<?php

// Require composer autoloader
require __DIR__ . '/vendor/autoload.php';

use App\Main;
use Bramus\Router\Router;

//error_reporting(E_ALL);

$app = new Main('https://dev-123formbuilder.us.auth0.com/', 'https://users-management-api');
//$app = new Main('https://dev-123formbuilder.us.auth0.com/', 'https://forms-management-api');

// Create Router instance
$router = new Router();

// Activate CORS
function sendCorsHeaders()
{
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: Authorization");
    header("Access-Control-Allow-Methods: GET,HEAD,PUT,PATCH,POST,DELETE");
}

$router->options('/.*', function () {
    sendCorsHeaders();
});

sendCorsHeaders();

// Check JWT on private routes
$router->before('GET', '/api/private.*', function () use ($app) {

    $requestHeaders = apache_request_headers();

    if (!isset($requestHeaders['authorization']) && !isset($requestHeaders['Authorization'])) {
        header('HTTP/1.0 401 Unauthorized');
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode(array("message" => "No token provided."));
        exit();
    }

    $authorizationHeader = isset($requestHeaders['authorization']) ? $requestHeaders['authorization'] : $requestHeaders['Authorization'];

    if ($authorizationHeader == null) {
        header('HTTP/1.0 401 Unauthorized');
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode(array("message" => "No authorization header sent."));
        exit();
    }

    $authorizationHeader = str_replace('bearer ', '', $authorizationHeader);
    $token = str_replace('Bearer ', '', $authorizationHeader);

    try {
        $app->setCurrentToken($token);
    } catch (\Auth0\SDK\Exception\CoreException $e) {
        header('HTTP/1.0 401 Unauthorized');
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode(array("message" => $e->getMessage()));
        exit();
    }
});

$router->get('/api/public', function () use ($app) {
    sendResponse('200 OK', $app->publicEndpoint());
});

$router->get('/api/private', function () use ($app) {
    sendResponse('200 OK', $app->privateEndpoint());
});

// Check for read:messages scope
$router->before('GET', '/api/private-scoped', function () use ($app) {
    if (!$app->checkScope('read:usersus')) {
        sendResponse('403 Forbidden', 'Insufficient scope.', true);
    }
});

$router->get('/api/private-scoped', function () use ($app) {
    sendResponse('200 OK', $app->privateScopedEndpoint());
});

$router->set404(function () {
    sendResponse('404 Not Found', 'Page not found.', true);
});

function sendResponse($httpStatus, $message, $error = false)
{
    header("HTTP/1.0 $httpStatus");
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode(['status' => $error ? 'error' : 'ok', 'message' => $message], JSON_PRETTY_PRINT);
    exit();
}

// Run the Router
$router->run();